class mpwar {


  $misc_packages = ['vim-enhanced', 'telnet', 'zip', 'unzip', 'git', 'screen', 'libssh2', 'libssh2-devel', 'gcc', 'gcc-c++', 'autoconf', 'automake']
  package { $misc_packages: ensure => latest }


  #APACHE WITH VIRTUAL HOSTS
  include apache

    apache::vhost { 'mympwar.prod':
        port          => '80',
        docroot       => '/var/www/prod',
        docroot_owner => 'vagrant',
        docroot_group => 'vagrant',
    }

    apache::vhost { 'mympwar.dev':
        port          => '80',
        docroot       => '/var/www/dev/',
        docroot_owner => 'vagrant',
        docroot_group => 'vagrant',
    }

    file { '/var/www':
      ensure => "directory",
    }
    file { '/var/www/prod':
      ensure => "directory",
      require => File['/var/www'],
    }
    file { '/var/www/dev':
      ensure => "directory",
      require => File['/var/www'],
    }
    file { '/var/www/dev/info.php':
      ensure => file,
      source => "puppet:///modules/mpwar/info.php",
      require => File['/var/www/dev'],
   }
    file { '/var/www/prod/index.php':
      ensure => file,
      content => "Hello World. Sistema operativo ${operatingsystem} ${operatingsystemrelease}",
      require => File['/var/www/prod'],
   }

  # MYSQL
  $databases = {
    'mpwar_test' => {
      ensure  => 'present',
      charset => 'utf8',
    },
    'mympwar' => {
      ensure  => 'present',
      charset => 'utf8',
    },
  }

  class { '::mysql::server':
    root_password    => 'mypassword',
    databases => $databases,
  }


  #PHP 5.5
  $yum_repo = 'remi-php55'
  include ::yum::repo::remi_php55

  class { 'php':
    version => 'latest',
    require => Yumrepo[$yum_repo]
  }

  php::module { [ 'devel', 'pear', 'mbstring', 'xml', 'tidy', 'pecl-memcache', 'soap' ]: }


  #FIREWALL, OPEN PORT 80
  include firewall

  firewall { '001 open port 80':
    chain     => 'INPUT',
    action    => 'accept',
    proto     => 'tcp',
    dport     => '80',
  }

  #MEMCACHED
  include memcached

  # Ensure Time Zone and Region.
  class { 'timezone':
      timezone => 'Europe/Madrid',
  }

  #NTP
  class { '::ntp':
    servers => [ '1.es.pool.ntp.org', '2.europe.pool.ntp.org', '3.europe.pool.ntp.org' ],
    package_ensure => 'latest'
  }

}
